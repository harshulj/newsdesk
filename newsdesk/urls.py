from django.conf.urls import patterns, include, url
from django.views.generic.simple import direct_to_template
from desk.views import create_article, show_profile, latest_news, activate

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'newsdesk.views.home', name='home'),
    # url(r'^newsdesk/', include('newsdesk.foo.urls')),
    url(r'^$', latest_news),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^$', direct_to_template, {'template': 'index.html'}),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^search/', include('haystack.urls')),
#    url(r'^accounts/
#    activate/(?P<activation_key>\w+)/$',
#                           activate,
#                           {'backend': 'registration.backends.default.DefaultBackend'},
#                           name='registration_activate'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/profile/$', show_profile),
    url(r'^news/new/$', create_article),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)

from registration.backends.simple import SimpleBackend

class RedirectBackend(SimpleBackend):
    def register(self, request, **kwargs):
        request.session['redirect'] = request.GET.get("next",None)
        super(RedirectBackend, self).register(request, **kwargs)

    def post_activation_redirect(self, request, user):
        return(request.session['redirect'], (), {})

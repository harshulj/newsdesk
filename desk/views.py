from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from desk.forms import ArticleForm
from desk.models import Article

def activate(self, request, activation_key):

    activated = RegistrationProfile.objects.activate_user(activation_key)
    if activated:
        signals.user_activated.send(sender=self.__class__,
                                    user=activated,
                                    request=request)
        return activated

def latest_news(request, template_name='index.html', extra_context=None):
    articles = Article.objects.order_by('created_on')

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    return render_to_response(template_name, {'articles': articles}, context_instance=context)

@login_required
def show_profile(request, template_name='profile.html', extra_context=None):
    articles = Article.objects.filter(author_id=request.user.author.id)

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    return render_to_response(template_name, {'articles' : articles}, context_instance=context)

@login_required
def create_article(request, success_url='/',  template_name='article_new.html', extra_context=None):
    if request.method == 'POST':
        article_form = ArticleForm(request.POST)
        if article_form.is_valid():
            link = article_form.save(commit=False)
            link.author = request.user.author
            link.save()
            return redirect(success_url)            
    else:
        article_form = ArticleForm()

    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    return render_to_response(template_name, {'article_form': article_form, } , context_instance=context)

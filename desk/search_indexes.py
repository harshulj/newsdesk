import datetime
from haystack import indexes
from desk.models import Article

class ArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    created_on = indexes.DateTimeField(model_attr='created_on')

    def get_model(self):
        return Article

    def index_queryset(self):
        return self.get_model().objects.filter()

from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    """
    """
    name = models.CharField(max_length=20)
    slug = models.SlugField(max_length=25)

    class Meta():
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.name

class Author(models.Model):
    """
    """
    short_bio = models.CharField(max_length=80, blank=True)
    about_me =  models.CharField(max_length=200, blank=True)
    user = models.OneToOneField(User)

    def __unicode__(self):
        return self.user.first_name + " " + self.user.last_name + "(" + self.user.username + ")"

    @property
    def name(self):
        return self.user.first_name+" " + self.user.last_name

class Tag(models.Model):
    """
    """
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Article(models.Model):
    """
    """
    headline = models.CharField(max_length=100)
    place = models.CharField(max_length=30)
    created_on = models.DateTimeField(auto_now_add=True)
    last_edited = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(Category, related_name='articles')
    author = models.ForeignKey(Author, related_name='articles')
    content = models.TextField()
    tags = models.ManyToManyField(Tag, related_name='articles', blank=True)
    slug = models.SlugField(max_length=110)

    def __unicode__(self):
        return self.headline

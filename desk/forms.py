from django import forms
from desk.models import Article, Author

class ArticleForm(forms.ModelForm):
    tags = forms.CharField(max_length=100, help_text="Enter comma seperated tags")
    
    class Meta:
        model = Article
        exclude = ('author',)
        widgets = {
            'headline' : forms.TextInput(attrs={'size':100}),
            'content'  : forms.Textarea(attrs={'colns': 20, 'rows':10}),
            'tags'     : forms.TextInput(),
            }
    def clean_tags(self):
        tag_line = self.cleaned_data['tags']

class AuthorForm(forms.Form):
    first_name = forms.CharField(max_length=50, label='First Name')
    last_name = forms.CharField(max_length=50, label='Last Name')
    short_bio = forms.CharField(max_length=80, label='Short Bio', help_text='This will be shown next to your name. Like "Programmer, Scientist etc."')
    about_me = forms.CharField(max_length=200, label='About Me', required=False)
    dob = forms.DateField(label='Date of Birth', help_text='We need this to confirm your age.')

    def save(self, user, commit=False):
        instance = super(AuthorForm, self).save(commit=False)
        if commit:
            author = Author.objects.get(id=user)

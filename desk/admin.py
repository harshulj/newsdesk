from desk.models import Article, Category, Author, Tag
from django.contrib import admin

admin.site.register(Article)
admin.site.register(Category)
admin.site.register(Author)
admin.site.register(Tag)
